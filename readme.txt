1. Verion : MS word LLB v1.0

2. Requirement : LabVIEW 2023 Q3 or later version

3. Update  History

v.1.1.1
   Modified : Close report applied for document closing.

v1.1
Basic Methods 
Advance Methods
	SubVIs			: Vertical Tab Constant
	Word.Range		: Set Rng PargrFrmt IndentCharWidth


v1.0
Basic Methods 
	Open		: Open WD, Open App, Open Dcument 
	Close		: Close WD
	Write Data	: Write Data Table Created, Write Data, Write Data Table
	Handle Table: Add Rows Columns, Create Table, Set Table BGcolors, Set Table widthHight
	Selects	: Select Cell(BMK), Select Table(BMK), Select Bookmark
	Utilities	: Show Font Wizard, set Visible
	Copy Paste	: Paste, Copy
	Save		: save
	Move Enter	: Type Enter, Move UpDwn Right Left
	New Page 	: Insert new page
	Insert Image File : Insert Image File

Advance Methods
	SubVIs			: Open Check
	Word._Application	: Add App Documents, Open App Documents, Convert App CmTo Points, Show App Font Dialog
	Word._Document		: Get Document Range, Save Document
	Word._Selection		: Insert PrgrphABF Selection, Move UpDwRL Selection, InsertNewPage Selection, Paste Selection, Copy Selection, Add RowColumn
	Word.Tables		: Add TAbles
	Word.Table			: Select Table Cell, Insert Table 2D Data, Set Table RowHeight, Set Table ColumnWidth, Set Table BgPattrnColor, Get Row Column Count
	Word.Range		: Select Range Table, Set Range Font, set Range Text, Get Range Start End
	Word.Font			: Set Font Name Size, Set Font Color, Set Font Bold, Set Font Itelic, Set Font Underline, Set Font StrikeThrough
	Word.InlineShapes	: AddPicture InlineShapes
	InlineShape		: InlineShape WidthHeight (Scal)